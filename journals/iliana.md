Journal Entries - IlianaJolie Avridor

Jul 17, 2023
Implemented signup forms to allow tenants and landlords to sign up. Created HTML templates for the signup forms, capturing essential user information like name, email, and password. Utilized FastAPI's form handling capabilities to process and store user data securely.


Jul 24, 2023
Debugged the signup forms to handle user input more robustly. Ensured data validation and added error handling to prevent invalid submissions. Implemented a user authentication system using FastAPI's built-in authentication capabilities. Secured the signup and login forms.


Jan 25, 2023
Enhanced the login form to include a role selection feature, enabling users to specify if they are a tenant or a landlord. Implemented this feature using HTML and JavaScript to manage the form dynamically and send the selected role to the backend for authentication.

Jul 26, 2023
Created an appointment history file for landlords to log their appointments. Designed the database schema to store appointment details like date, time, description, and associated landlord information. Developed the backend API endpoints for the appointment history, enabling landlords to add, view, and manage their appointments. Implemented the necessary CRUD (Create, Read, Update, Delete) operations for appointment management.Wrote unit tests for the appointment history endpoints to ensure the proper functioning of the API and to maintain code reliability.


Jun 27, 2023
Refactored the appointment history file to improve code organization and readability. Added API documentation to provide clear instructions on using the appointment history endpoints.Conducted end-to-end testing for the appointment history feature, ensuring seamless integration with the frontend and identifying and resolving any potential issues.


Throughout the Project:
Collaborated with the team to review and align on the latest update wireframes and designs, ensuring consistent and cohesive user experience across the application. Actively participated in regular team discussions, providing valuable insights and contributions to enhance the overall project quality. Continuously learned and researched best practices in web development, security, and user experience to implement cutting-edge features and deliver an exceptional application.
